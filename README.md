# Minesweeper 💣

An implementation of the Minesweeper game for the MZ_APO kit based on [MicroZed](http://microzed.org/product/microzed) board.

For more information, please see [the full documentation page](src/README.md).

Developed as semestral project for APO course at FEE CTU by Viktorie Valdmanová and Petr Netušil,
currently completed. 

Based on [MZ_APO project template](https://gitlab.fel.cvut.cz/b35apo/mzapo_template).

Licensed under MIT license.
