# Minesweeper 💣

An implementation of the Minesweeper game for the MZ_APO kit based on [MicroZed](http://microzed.org/product/microzed) board.


Developed as semestral project for APO course at FEE CTU by Viktorie Valdmanová and Petr Netušil,
currently completed. 

Based on [MZ_APO project template](https://gitlab.fel.cvut.cz/b35apo/mzapo_template).

Licensed under MIT license.

*** 

## Installation guide

Supported installation, compilation and upload (via attached Makefile, `src/Makefile`) are 
described in this 
guide.

However, feel free to make things work however you want.

### 1. Crosscompiling and running  from another computer

### Prerequisites
#### Software
You will need the following software to be installed on your computer in order to build this 
program.

1. GNU C11 compatible ARM compiler
   - package `crossbuild-essential-armhf` on Ubuntu/Debian systems
   - package [`arm-linux-gnueabihf-gcc`](https://aur.archlinux.org/packages/arm-linux-gnueabihf-gcc) available on [AUR](https://aur.archlinux.org/)
2. GNU Make
3. SSH tool (both on your computer and the MicroZed board)

#### Network connection
You will also need to be able to connect to the board via IP protocols.

### Makefile configuration
#### SSH setup
This guide uses SCP (which is based on SSH) to transfer the compiled executable to 
the board. 

If you intend to use SSH key to connect to the board (which is the default), please make sure that:
1. you have a key named `mzapo-root-key` in `~/.ssh` directory
2. or you have adjusted path to the key (at line 27 of the Makefile)

If you don't wish to authenticate by password instead, please comment out line 27 of the 
Makefile instead.

##### Using locally
You will need to set `TARGET_IP` to the current IP address of your board on line 21 of the Makefile.

##### Using `postel.felk.cvut.cz` proxy
You will need to set `SSH_PROXY` to 1 on line 26 of the makefile and set `TARGET_IP` to the 
current IP address of your board on line 29.

You will also need to configure the proxy by either adding new user entry or adjusting an existing 
one. Template snippet for the entry is shown below.
```asm
ifeq ($(shell whoami), pc_username)
SSH_OPTIONS += -o 'ProxyJump=ctu_username@postel.felk.cvut.cz'
endif
```

### Runing the program
Now the program be run on the board by running `make run` command from `src` directory.

### 2. Compiling and running directly from the board
### Prerequisites
#### Software
You will need the following software to be installed on your board.

1. GNU C11 compatible ARM compiler
   - package `crossbuild-essential-armhf` on Ubuntu/Debian systems
   - package [`arm-linux-gnueabihf-gcc`](https://aur.archlinux.org/packages/arm-linux-gnueabihf-gcc) available on [AUR](https://aur.archlinux.org/)
2. GNU Make


### Runing the program
Now the program be run on the board by running `make` command from `src` directory and then 
running compiled program `./main`.

***

## User guide
The program consists of three screens as described bellow. User interface is self-documented 
through the whole program.

### Main menu screen
The main menu can be navigated horizontally by a red and vertically by the green knob.

Users can choose to read the manual, start the game, or exit the application.

The game can be played in three different difficulties that adjust the amount of mines on the 
board:
- EASY, 8 mines;
- NORMAL, 10 mines;
- HARD, 12 mines.


<img src="https://gitlab.fel.cvut.cz/netuspet/minesweeper/-/raw/dev/doc/img/main.jpg" width="484" height="325">

### Game screen

#### Game controlls
User can control the board using red and blue knob.

X coordinate can be set by the red knob and Y coordinate by blue knob.

On red knob click current position is revealed. On blue knob click current position is marked by flag.
#### Side menu
The side menu can be navigated by the green knob. User can choose to play a new game or exit the application

<img src="https://gitlab.fel.cvut.cz/netuspet/minesweeper/-/raw/dev/doc/img/ingame.jpg" width="484" height="325">

<img src="https://gitlab.fel.cvut.cz/netuspet/minesweeper/-/raw/dev/doc/img/win.jpg" width="484" height="325">

<img src="https://gitlab.fel.cvut.cz/netuspet/minesweeper/-/raw/dev/doc/img/lose.jpg"  width="484" height="325">

### Tutorial screen
Tutorial screen contains a written guide on how to play the game.

***

<img src="https://gitlab.fel.cvut.cz/netuspet/minesweeper/-/raw/dev/doc/img/manual.jpg" width="484" height="325">


## Technical documentation

The basic structure of the project with brief description is documented on the following diagram:

<img src="https://gitlab.fel.cvut.cz/netuspet/minesweeper/-/raw/dev/doc/img/basic_structure.png" width="484" height="325">


Full technical documentation can be generated via [Doxygen](https://www.doxygen.nl/index.html) by the 
configuration script `doc/Doxyfile`.

After installation of Doxygen, run the following command from root directory of this repository.
```
doxygen doc/Doxyfile   
```
This will generate `doc/html` and `doc/latex` directories.

Web-based documentation will be accessible through `doc/htmlindex.html` file.
To obtain a PDF file with the documentation, one must compile it from [LaTeX](https://www.latex-project.org/) sources. Makefile for the compilation will be available in `doc/latex` 
directory.
