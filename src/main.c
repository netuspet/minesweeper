/*******************************************************************
  Project main function template for MicroZed based MZ_APO board
  designed by Petr Porazil at PiKRON

  main.c      - main file

  @author Viktorie Valdmanova
  @author Petr Netusil

  Licensed under MIT license.

 *******************************************************************/

#define _POSIX_C_SOURCE 200112L

#include "serialize_lock.h"
#include <stdio.h>
#include <time.h>

#include "visual_config.h"
#include "peripherals/knobs/knob_utils.h"
#include "peripherals/lcd/lcd_utils.h"
#include "peripherals/led/led_utils.h"
#include "screens/screen_utils.h"

#define FRAME_DELAY_MS 150


int main(int argc, char *argv[]) {
    if (serialize_lock(1) <= 0) {
        printf("System is occupied, waiting...\n");
        serialize_lock(0);
    }

    MemBase mem_base;
    if (init_mem_base(&mem_base) == false) {
        fprintf(stderr, "Unable map peripheries to memory, exiting.\n");
        exit(EXIT_FAILURE);
    }

    Color LCD_buffer[LCD_WIDTH * LCD_HEIGHT];

    Knobs knobs;
    init_knobs(&knobs, mem_base.phys_mem_base);

    init_leds(mem_base.phys_mem_base);

    line_from_out_in(17, LED_EFFECT_DELAY_MS);

    init_main_menu();

    struct timespec loop_delay = {FRAME_DELAY_MS / 1000, (FRAME_DELAY_MS % 1000) * 1000 * 1000};

    while (true) {
        update_knobs(&knobs);
        update_screen(&knobs, LCD_buffer);
        render_buffer(LCD_buffer, mem_base.lcd_mem_base);
        clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
    }
    // exit handled via exit screen
}
