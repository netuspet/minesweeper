#ifndef MINESWEEPER_BOARD_H
#define MINESWEEPER_BOARD_H

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "../time/stopwatch.h"

#define BOARD_WIDTH 8

typedef int8_t FieldData;

typedef enum FieldStatus {
     KNOWN = 1,
     UNKNOWN = 0,
     MARKED = 2
} FieldStatus;

typedef struct Cell {
     bool revealed;
     bool marked;
     bool mine;
     uint8_t num_neighbour_mines;
} Cell;

typedef enum GameStatus {
     WON,
     GAME_ON,
     LOST
} GameStatus;

void setup_board(uint8_t num_mines);

void reveal_position(uint8_t x, uint8_t y);

void mark_position(uint8_t x, uint8_t y);

void reveal_everything();

Cell* get_board();

int get_remaining_flags();

bool is_game_over();

GameStatus get_game_status();

#endif//MINESWEEPER_BOARD_H
