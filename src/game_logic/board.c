/*******************************************************************
  board.c      - contains game logic

  @author Viktorie Valdmanova
  @author Petr Netusil

  Licensed under MIT license.

 *******************************************************************/

#include "board.h"

void put_mines_onboard(uint8_t num_mines);
void set_cell_numbers();

Cell board[BOARD_WIDTH * BOARD_WIDTH];
GameStatus game_status;

int not_revealed_left;
int flags_remaining;
int mines_remaining;

/**
 * @brief Setups board for the game.
 * @param num_mines Number of mines on the board.
 */
void setup_board(uint8_t num_mines) {
     game_status = GAME_ON;

     not_revealed_left = BOARD_WIDTH * BOARD_WIDTH;
     mines_remaining = num_mines;
     flags_remaining = num_mines;

     for (int i = 0; i < BOARD_WIDTH; i++) {
          for (int j = 0; j < BOARD_WIDTH; j++) {
               board[i * BOARD_WIDTH + j].marked = false;
               board[i * BOARD_WIDTH + j].revealed = false;
               board[i * BOARD_WIDTH + j].mine = false;
               board[i * BOARD_WIDTH + j].num_neighbour_mines = 0;
          }
     }

     put_mines_onboard(num_mines);
     set_cell_numbers();
}

/**
 * @brief Reaveals cell at given position. It will reveal all cells around if on none of them is a mine.
 * @param x X coordinate.
 * @param y Y coordinate.
 */
void reveal_position(uint8_t x, uint8_t y) {
     if(!board[y * BOARD_WIDTH + x].revealed) {

          board[y * BOARD_WIDTH + x].revealed = true;
          not_revealed_left--;

          if(board[y * BOARD_WIDTH + x].mine) {
               game_status = LOST;
               stop_stopwatch();
          } else if(not_revealed_left <= mines_remaining) {
               game_status = WON;
               stop_stopwatch();
          } else if(!board[y * BOARD_WIDTH + x].mine && board[y * BOARD_WIDTH + x].num_neighbour_mines == 0) {
               for(int i = -1; i <= 1; i++) {
                    for (int j = -1; j <= 1; j++) {
                         if(y + i >= 0 && y + i < BOARD_WIDTH && x + j >= 0 && x + j < BOARD_WIDTH) {
                              reveal_position(x + j, y + i);
                         }
                    }
               }
          }
     }
}

/**
 * @brief Marks cell at given position.
 * @param x X coordinate.
 * @param y Y coordinate.
 */
void mark_position(uint8_t x, uint8_t y) {
     if(!board[y * BOARD_WIDTH + x].marked) {
         flags_remaining--;
         not_revealed_left--;
          if(board[y * BOARD_WIDTH + x].mine) {
              mines_remaining--;

              if(mines_remaining == 0) {
                    game_status = WON;
                    stop_stopwatch();
              }
          }
     } else {
          flags_remaining++;
          not_revealed_left++;
          if(board[y * BOARD_WIDTH + x].mine) {
              mines_remaining++;
          }
     }

     board[y * BOARD_WIDTH + x].marked = !board[y * BOARD_WIDTH + x].marked;
}

/**
 * @brief Reveals whole board.
 */
void reveal_everything() {
     for (int i = 0; i < BOARD_WIDTH; i++) {
          for (int j = 0; j < BOARD_WIDTH; j++) {
               board[i * BOARD_WIDTH + j].revealed = true;
          }
     }
}

/**
 * @brief Puts mines on the board at random positions.
 * @param num_mines Number of mines.
 */
void put_mines_onboard(uint8_t num_mines) {
     int placed = 0;
     srand(time(NULL));

     while (placed < num_mines) {
          int x = rand() % BOARD_WIDTH;
          int y = rand() % BOARD_WIDTH;

          if (board[y * BOARD_WIDTH + x].mine == false) {
               board[y * BOARD_WIDTH + x].mine = true;
               placed++;
          }
     }
}

/**
 * @brief Calculates number of mines around cells.
 */
void set_cell_numbers() {
     for (int y = 0; y < BOARD_WIDTH; y++) {
          for (int x = 0; x < BOARD_WIDTH; x++) {

               if (board[y * BOARD_WIDTH + x].mine == false) {
                    int neighbours_count = 0;
                    for(int i = -1; i <= 1; i++) {
                         for (int j = -1; j <= 1; j++) {
                              if(x + j >= 0 && x + j < BOARD_WIDTH && y + i >= 0 && y + i < BOARD_WIDTH) {
                                   if(board[(y + i) * BOARD_WIDTH + (x + j)].mine) {
                                        neighbours_count++;
                                   }
                              }
                         }
                    }
                    board[y * BOARD_WIDTH + x].num_neighbour_mines = neighbours_count;
               } else {
                    board[y * BOARD_WIDTH + x].num_neighbour_mines = 9;
               }
          }
     }
}


/**
 * @brief Returns game status.
 * @return Game status.
 */
GameStatus get_game_status() {
     return game_status;
}

/**
 * @brief Returns number of flags left.
 * @return Number of flags left.
 */
int get_remaining_flags() {
     return flags_remaining;
}

/**
 * @brief Returns game board.
 * @return Game board.
 */
Cell* get_board() {
    return board;
}

