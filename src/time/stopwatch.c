/*******************************************************************
  stopwatch.c      - measures game time

  @author Viktorie Valdmanova
  @author Petr Netusil

  Licensed under MIT license.

 *******************************************************************/

#include "stopwatch.h"

time_t start_timestamp;
time_t stop_timestamp;


/**
 * @brief Sets start timestamp.
 */
void start_stopwatch() {
     start_timestamp = time(0);
     stop_timestamp = 0;
}

/**
 * @brief Sets elapsed time to string buffer.
 * @param time_str String buffer.
 */
void get_elapsed_time(char *time_str) {
     double elapsed_time;
     if(!stop_timestamp) {
          elapsed_time = difftime(time(0), start_timestamp);
     } else {
          elapsed_time = difftime(stop_timestamp, start_timestamp);
     }

     uint8_t seconds = (uint8_t)(elapsed_time) % 60;
     uint8_t minutes = (uint8_t)(elapsed_time / 60) % 60;

     sprintf(time_str, "Time: %02d:%02d", minutes, seconds);
}

/**
 * @brief Sets stop timestamp.
 */
void stop_stopwatch() {
     stop_timestamp = time(0);
}