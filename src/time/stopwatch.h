#ifndef MINESWEEPER_STOPWATCH_H
#define MINESWEEPER_STOPWATCH_H

#include <stdio.h>
#include <stdint.h>
#include <time.h>

void start_stopwatch();

void get_elapsed_time(char *time_str);

void stop_stopwatch();


#endif //MINESWEEPER_STOPWATCH_H