/*******************************************************************
  visual_config.h      - file with configuration of fonts, colors,
                         margins, paddings etc.

  @author Viktorie Valdmanova
  @author Petr Netusil

  Licensed under MIT license.

 *******************************************************************/

#ifndef MINESWEEPER_VISUAL_CONFIG_H
#define MINESWEEPER_VISUAL_CONFIG_H

/* Delays */

#define LED_EFFECT_DELAY_MS 100         /* LED (line) effects blink time */
#define LED_EFFECT_DELAY_LONG_MS 400    /* LED RGB blink time */

/* Fonts */

#define PROPORTIONAL_FONT font_rom8x16                  /* Proportional font used in the project */
#define UNPROPORTIONAL_FONT font_winFreeSystem14x16     /* Non-proportional font used in the project */

/* Font sizes */

#define HUGE_FONT_SCALE 3
#define BIG_FONT_SCALE 2
#define NORMAL_FONT_SCALE 1

/* Margins, paddings, borders */

#define MENU_LEFT_MARGIN 40
#define TEXT_MARGIN_TOP 15

#define BUTTON_MARGIN_X 15

#define BUTTON_PADDING_X 20
#define BUTTON_PADDING_Y 10

#define BUTTON_BORDER_WIDTH 2

/* Colors */

#define HIGHLIGHT_TEXT_COLOR YELLOW
#define TEXT_COLOR WHITE

#define BACKGROUND_COLOR GRAY

#define CELL_COLOR LIGHTGRAY

#endif//MINESWEEPER_VISUAL_CONFIG_H
