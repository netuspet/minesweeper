/*******************************************************************
  menu_screen.c      - contains difficulty settings

  This screen serves as the main menu, provides settings and
  routes to all other screens.

  @author Viktorie Valdmanova
  @author Petr Netusil

  Licensed under MIT license.

 *******************************************************************/

#include "menu_screen.h"

/* Difficulty settings */
typedef enum GameDifficulty {
    EASY = 8,
    NORMAL = 10,
    HARD = 12,
} GameDifficulty;

static uint8_t current_difficulty = EASY;

/* Button callbacks */
static void difficulty_easy_callback() { current_difficulty = EASY; }
static void difficulty_normal_callback() { current_difficulty = NORMAL; }
static void difficulty_hard_callback() { current_difficulty = HARD; }
static void tutorial_callback() { init_tutorial_screen(); }
static void exit_callback() { init_exit_screen(); }
static void run_callback() {
    setup_board(current_difficulty); 
    start_stopwatch();
    init_game_screen();
}

/* Button definition */
static const MenuButton difficulty_easy_button = {"Easy", EASY, &difficulty_easy_callback};
static const MenuButton difficulty_normal_button = {"Normal", NORMAL, &difficulty_normal_callback};
static const MenuButton difficulty_hard_button = {"Hard", HARD, &difficulty_hard_callback};
static const MenuButton exit_button = {"Exit", -1, &exit_callback};
static const MenuButton tutorial_button = {"How to play", -1, &tutorial_callback};
static const MenuButton run_game_button = {"Start the game", -1, &run_callback};

/* Menu definition */
static const MenuButton *row0_buttons[] = {&difficulty_easy_button, &difficulty_normal_button, &difficulty_hard_button};
static const MenuButton *row1_buttons[] = {&exit_button, &tutorial_button, &run_game_button};
static const MenuButton **buttons[] = {&row0_buttons[0], &row1_buttons[0]};

static ButtonArea menu = {
        .row_amount = 2,
        .columns_in_row = (uint8_t[]){3, 3},
        .buttons = buttons,
};

/* Cursor definition */
static CursorPosition menu_cursor = {
        .x = 0,
        .y = 0
};

/* Extern functions */

/**
 * @brief Handles cursor movement and callbacks calling upon user interaction.
 * @param knobs Knobs struct to read input from.
 */
void handle_input_main_menu(Knobs *knobs) {
    if (knobs->G_click) {
        menu.buttons[menu_cursor.y][menu_cursor.x]->callback();
    }

    move_cursor_y(&menu_cursor, knobs->G_change, menu.row_amount);
    move_cursor_x(&menu_cursor, knobs->R_change, menu.columns_in_row[menu_cursor.y]);
}

/**
 * @brief Renders the screen contents into a buffer.
 * @param LCD_buffer A pointer to the buffer to draw to.
 */
void draw_main_menu(Color *LCD_buffer) {
    // draw background
    draw_mono(LCD_buffer, BACKGROUND_COLOR);

    XY position = {MENU_LEFT_MARGIN, 0};

    // draw title
    position.y += TEXT_MARGIN_TOP * HUGE_FONT_SCALE;
    position.x += draw_string(LCD_buffer, &UNPROPORTIONAL_FONT, "Minesweeper", position, TEXT_COLOR, HUGE_FONT_SCALE);
    position.x += MENU_LEFT_MARGIN;
    draw_icon(LCD_buffer, position, get_icon(MINE), HUGE_FONT_SCALE);

    // first line
    position.x = MENU_LEFT_MARGIN;
    position.y += UNPROPORTIONAL_FONT.height * HUGE_FONT_SCALE + TEXT_MARGIN_TOP * NORMAL_FONT_SCALE;
    position.x += draw_string(LCD_buffer, &UNPROPORTIONAL_FONT, "Use the ", position, TEXT_COLOR, NORMAL_FONT_SCALE);
    position.x += draw_string(LCD_buffer, &UNPROPORTIONAL_FONT, "red", position, RED, NORMAL_FONT_SCALE);
    draw_string(LCD_buffer, &UNPROPORTIONAL_FONT, " knob for horizontal scrolling.", position, TEXT_COLOR, NORMAL_FONT_SCALE);
    // second line
    position.x = MENU_LEFT_MARGIN;
    position.y += (UNPROPORTIONAL_FONT.height + TEXT_MARGIN_TOP) * NORMAL_FONT_SCALE;
    position.x += draw_string(LCD_buffer, &UNPROPORTIONAL_FONT, "Use the ", position, TEXT_COLOR, NORMAL_FONT_SCALE);
    position.x += draw_string(LCD_buffer, &UNPROPORTIONAL_FONT, "green", position, GREEN, NORMAL_FONT_SCALE);
    draw_string(LCD_buffer, &UNPROPORTIONAL_FONT, " knob for clicking and vertical menu scrolling.", position, TEXT_COLOR, NORMAL_FONT_SCALE);

    // draw buttons
    position.x = MENU_LEFT_MARGIN;
    position.y += UNPROPORTIONAL_FONT.height * NORMAL_FONT_SCALE + TEXT_MARGIN_TOP * NORMAL_FONT_SCALE;
    position.x += draw_string(LCD_buffer, &UNPROPORTIONAL_FONT, "Difficulty:", (XY){position.x, position.y + BUTTON_PADDING_Y}, TEXT_COLOR, NORMAL_FONT_SCALE);
    position.x += BUTTON_MARGIN_X;
    for (uint8_t r = 0; r < menu.row_amount; ++r) {
        for (uint8_t c = 0; c < menu.columns_in_row[r]; ++c) {
            const MenuButton *button = menu.buttons[r][c];
            position.x += draw_button(LCD_buffer, &UNPROPORTIONAL_FONT, button->label, position, (button->value == current_difficulty) ? HIGHLIGHT_TEXT_COLOR : TEXT_COLOR, NORMAL_FONT_SCALE, c == menu_cursor.x && r == menu_cursor.y);
            position.x += BUTTON_MARGIN_X;
        }
        position.y += TEXT_MARGIN_TOP * NORMAL_FONT_SCALE + 2 * BUTTON_PADDING_Y + UNPROPORTIONAL_FONT.height * NORMAL_FONT_SCALE;
        position.x = MENU_LEFT_MARGIN;
    }
}
