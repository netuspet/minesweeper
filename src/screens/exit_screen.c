/*******************************************************************
  exit_screen.c      - contains rendering and input handling of
                       MINESWEEPER_EXIT_SCREEN_H

  @author Viktorie Valdmanova
  @author Petr Netusil

  Licensed under MIT license.

 *******************************************************************/

#include "exit_screen.h"

/**
 * @brief Blinks LED line, calls 'mzapo_lcdip' and exits.
 * @param knobs Pointer to the Knobs structure to read input from.
 */
void handle_input_exit_screen(Knobs *knobs) {
    line_from_in_out(17, LED_EFFECT_DELAY_MS);
    int status = system("/opt/apo/lcd/mzapo_lcdip/mzapo_lcdip");
    serialize_unlock();
    exit(status);
}

/**
 * @brief This function body is empty.
 * @param LCD_buffer Buffer to draw the screen to.
 */
void draw_exit_screen(Color *LCD_buffer) {
    // intentionally empty
}