#ifndef MINESWEEPER_GAME_SCREEN_H
#define MINESWEEPER_GAME_SCREEN_H

#include "screen_utils.h"
#include "../peripherals/lcd/lcd_utils.h"
#include "../peripherals/lcd/text.h"
#include "../peripherals/lcd/icon.h"
#include "../peripherals/led/led_utils.h"
#include "../peripherals/knobs/knob_utils.h"
#include "../fonts/font_types.h"
#include "../game_logic/board.h"
#include "../time/stopwatch.h"
#include "../visual_config.h"

#define BOARD_PADDING_PX 20
#define BOARD_SIZE_PX 280
#define CELL_SIZE_PX 35
#define BORDER_WIDTH_PX 2
#define INNER_CELL_SIZE_PX 31
#define STR_BUFFER_SIZE 20


void handle_input_game_screen(Knobs *knobs);

void draw_game_screen(Color *LCD_buffer);

#endif//MINESWEEPER_GAME_SCREEN_H
