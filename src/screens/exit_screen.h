#ifndef MINESWEEPER_EXIT_SCREEN_H
#define MINESWEEPER_EXIT_SCREEN_H

#include "screen_utils.h"
#include "../peripherals/lcd/text.h"
#include "../peripherals/knobs/knob_utils.h"
#include "../peripherals/lcd/lcd_utils.h"
#include "../peripherals/led/led_utils.h"
#include "../visual_config.h"
#include "../serialize_lock.h"

void handle_input_exit_screen(Knobs *knobs);

void draw_exit_screen(Color *LCD_buffer);

#endif//MINESWEEPER_EXIT_SCREEN_H
