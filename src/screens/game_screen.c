/*******************************************************************
  game_screen.c      - contains rendering and input handling of
                       MINESWEEPER_GAME_SCREEN_H

  This screen handles all interactions during game and provides
  a simple menu to start a new game or exit the program.

  UI layout:
  -------------------------------------------
  | --------------------                    |
  | |                  |                    |
  | |  BOARD AREA      |  MENU AREA         |
  | |  - (Y) blue knob |  - (Y) green knob  |
  | |  - (X) red knob  |                    |
  | |                  |                    |
  | --------------------                    |
  |_________________________________________|


  @author Viktorie Valdmanova
  @author Petr Netusil

  Licensed under MIT license.

 *******************************************************************/

#include "game_screen.h"

void new_game_callback() {
    init_main_menu();
}

void exit_callback() {
    init_exit_screen();
}

void on_game_lost() {
    reveal_everything();
    blink_rgbs(0xFF0000, LED_EFFECT_DELAY_LONG_MS);
}

void on_game_won() {
    reveal_everything();
    blink_rgbs(0x00FF00, 2 * LED_EFFECT_DELAY_LONG_MS);
}

/* Buttons included in this screen's UI. */
static const MenuButton exit_button = {"Exit", -1, &exit_callback};
static const MenuButton new_game_button = {"New game", -1, &new_game_callback};

/* Definition of the menu and it's structure. */
static const MenuButton *row0_buttons[] = {&new_game_button};
static const MenuButton *row1_buttons[] = {&exit_button};
static const MenuButton **buttons[] = {&row0_buttons[0], &row1_buttons[0]};

static ButtonArea menu = {
        .row_amount = 2,
        .columns_in_row = (uint8_t[]){1, 1},
        .buttons = buttons,
};

/* Cursors for each section of the UI. */
static CursorPosition menu_cursor = {
        .x = 0,
        .y = 0
};

static CursorPosition board_cursor = {
        .x = 0,
        .y = 0
};

/* Utility functions */
/**
 * @brief Converts FieldData aka int8_t to char (to be rendered).
 * @param data FieldData to be converted.
 * @return Data as char.
 */
static char get_char_from_data(FieldData data) {
    return data + 48;
}

/**
 * Computes offset when centering obj in length.
 * @param len_size Size to fit the object in.
 * @param obj_size Size of the object to be fitted.
 * @return Computed offset from centered position.
 */
static uint8_t get_text_offset(uint8_t len_size, uint8_t obj_size) {
    return (len_size - obj_size) / 2;
}

/* Draw functions */
static void draw_board_cursor(Color *LCD_buffer) {
    XY position = {BOARD_PADDING_PX + board_cursor.x * CELL_SIZE_PX, BOARD_PADDING_PX + board_cursor.y * CELL_SIZE_PX};
    draw_box(LCD_buffer, position, HIGHLIGHT_TEXT_COLOR, (XY){CELL_SIZE_PX, CELL_SIZE_PX}, BORDER_WIDTH_PX);
}

static void draw_cell(Color *LCD_buffer, XY position, Cell *cell) {
    position.x += BORDER_WIDTH_PX;
    position.y += BORDER_WIDTH_PX;
    draw_point(LCD_buffer, position, CELL_COLOR, INNER_CELL_SIZE_PX);
    if (cell->revealed && !cell->mine) {
        char ch = get_char_from_data(cell->num_neighbour_mines);
        uint8_t x_offset = get_text_offset(INNER_CELL_SIZE_PX, (uint8_t)(get_char_width(&PROPORTIONAL_FONT, ch) * NORMAL_FONT_SCALE));
        uint8_t y_offset = get_text_offset(INNER_CELL_SIZE_PX, (uint8_t)(PROPORTIONAL_FONT.height * NORMAL_FONT_SCALE));
        draw_char(LCD_buffer, &PROPORTIONAL_FONT, ch, (XY){position.x + x_offset, position.y + y_offset}, HIGHLIGHT_TEXT_COLOR, NORMAL_FONT_SCALE);
    }
    else {
        const Icon *icon = NULL;
        if (cell->marked) icon = get_icon(FLAG);
        else if (get_game_status() != GAME_ON && cell->mine) icon = get_icon(MINE);
        if (icon) {
            uint8_t x_offset = icon->width;
            uint8_t y_offset = get_text_offset(INNER_CELL_SIZE_PX, (uint8_t) (icon->height));
            draw_icon(LCD_buffer, (XY){position.x + x_offset, position.y + y_offset}, icon, 1);
        }
    }
}

static void draw_board(Color *LCD_buffer, XY position, Cell *board) {
    for (uint16_t y = 0; y < BOARD_WIDTH; ++y) {
        for (uint16_t x = 0; x < BOARD_WIDTH; ++x) {
            draw_cell(LCD_buffer, position, &board[y * BOARD_WIDTH + x]);
            position.x += CELL_SIZE_PX;
        }
        position.x = BOARD_PADDING_PX;
        position.y += CELL_SIZE_PX;
    }
}

static void draw_menu(Color *LCD_buffer, XY position) {
    uint16_t initial_x = position.x;



    if (get_game_status() != GAME_ON) {
        if(get_game_status() == WON) {
            draw_string(LCD_buffer, &UNPROPORTIONAL_FONT, "You Win!", position, GREEN, BIG_FONT_SCALE);
        } else if (get_game_status() == LOST) {
            draw_string(LCD_buffer, &UNPROPORTIONAL_FONT, "Game Lost!", position, RED, BIG_FONT_SCALE);
        }
        
        position.y += TEXT_MARGIN_TOP * BIG_FONT_SCALE + UNPROPORTIONAL_FONT.height * BIG_FONT_SCALE;
    }

    for (uint8_t r = 0; r < menu.row_amount; ++r) {
        for (uint8_t c = 0; c < menu.columns_in_row[r]; ++c) {
            const MenuButton *button = menu.buttons[r][c];
            position.x += draw_button(LCD_buffer, &UNPROPORTIONAL_FONT, button->label, position, TEXT_COLOR, NORMAL_FONT_SCALE, c == menu_cursor.x && r == menu_cursor.y);
            position.x += BUTTON_MARGIN_X;
        }
        position.y += TEXT_MARGIN_TOP * NORMAL_FONT_SCALE + 2 * BUTTON_PADDING_Y + UNPROPORTIONAL_FONT.height * NORMAL_FONT_SCALE;
        position.x = initial_x;
    }

    char str_buffer[STR_BUFFER_SIZE];
    get_elapsed_time(str_buffer);
    draw_string(LCD_buffer, &UNPROPORTIONAL_FONT, str_buffer, position, TEXT_COLOR, NORMAL_FONT_SCALE);
    position.y += TEXT_MARGIN_TOP * NORMAL_FONT_SCALE + UNPROPORTIONAL_FONT.height * NORMAL_FONT_SCALE;
    sprintf(str_buffer, "Mines left: %d", get_remaining_flags());
    draw_string(LCD_buffer, &UNPROPORTIONAL_FONT, str_buffer, position, TEXT_COLOR, NORMAL_FONT_SCALE);
}

/* Extern functions */
void handle_input_game_screen(Knobs *knobs) {
    if (knobs->G_click) {
        menu.buttons[menu_cursor.y][menu_cursor.x]->callback();
    }


    if(get_game_status() == GAME_ON && (knobs->R_click || knobs->B_click)) {
        if (knobs->R_click) {
            reveal_position(board_cursor.x, board_cursor.y);
        }
        if (knobs->B_click) {
            mark_position(board_cursor.x, board_cursor.y);
        }

        if(get_game_status() == WON) {
            on_game_won();
        } else if(get_game_status() == LOST) {
            on_game_lost();
        }
    }


    move_cursor_y(&board_cursor, -knobs->B_change, BOARD_WIDTH);
    move_cursor_x(&board_cursor, -knobs->R_change, BOARD_WIDTH);
    move_cursor_y(&menu_cursor, knobs->G_change, menu.row_amount);
}

void draw_game_screen(Color *LCD_buffer) {
    draw_mono(LCD_buffer, BACKGROUND_COLOR);
    draw_board(LCD_buffer, (XY){BOARD_PADDING_PX, BOARD_PADDING_PX}, get_board());
    if (get_game_status() == GAME_ON) draw_board_cursor(LCD_buffer);
    draw_menu(LCD_buffer, (XY){BOARD_PADDING_PX + BOARD_SIZE_PX + BUTTON_MARGIN_X, BOARD_PADDING_PX});
}

