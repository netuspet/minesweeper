#ifndef MINESWEEPER_MENU_SCREEN_H
#define MINESWEEPER_MENU_SCREEN_H

#include "../fonts/font_types.h"
#include "../peripherals/lcd/text.h"
#include "../peripherals/knobs/knob_utils.h"
#include "../peripherals/lcd/icon.h"
#include "../peripherals/lcd/lcd_utils.h"
#include "../visual_config.h"
#include "../game_logic/board.h"
#include "../time/stopwatch.h"
#include "screen_utils.h"


void handle_input_main_menu(Knobs *knobs);

void draw_main_menu(Color *LCD_buffer);

#endif//MINESWEEPER_MENU_SCREEN_H
