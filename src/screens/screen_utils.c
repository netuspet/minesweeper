/*******************************************************************
  screen_utils.c      - interface for handling individual screens
                        with abstraction

  @author Viktorie Valdmanova
  @author Petr Netusil

  Licensed under MIT license.

 *******************************************************************/


#include "screen_utils.h"
#include "menu_screen.h"
#include "game_screen.h"
#include "exit_screen.h"
#include "tutorial_screen.h"

static Screen current_screen; /* static declaration of the used Screen object */

/**
 * @brief Handles cursor movement on the Y axis.
 *        The cursor is bound between 0 (inclusive) and max (non-inclusive).
 *        Supports under/overflows of the cursor.
 * @param cursor Cursor struct to evaluate input for.
 * @param change Change of the knob.
 * @param max Amount of cursor steps.
 */
void move_cursor_y(CursorPosition *cursor, int8_t change, uint8_t max) {
    if (change > 0) {
        cursor->y -= 1;
        cursor->y %= max;
    }
    else if (change < 0) {
        if (cursor->y == 0)  cursor->y = max;
        cursor->y += 1;
        cursor->y %= max;
    }
}

/**
 * @brief Handles cursor movement on the X axis.
 *        The cursor is bound between 0 (inclusive) and max (non-inclusive).
 *        Supports under/overflows of the cursor.
 * @param cursor Cursor struct to evaluate input for.
 * @param change Change of the knob.
 * @param max Amount of cursor steps.
 */
void move_cursor_x(CursorPosition *cursor, int8_t change, uint8_t max) {
    if (change > 0) {
        cursor->x += 1;
        cursor->x %= max;
    }
    else if (change < 0) {
        if (cursor->x == 0) cursor->x = max;
        cursor->x -= 1;
        cursor->x %= max;
    }
    else {
        cursor->x %= max;
    }
}

/**
 * @brief Draws a button into a buffer.
 * @param LCD_buffer Buffer to draw to.
 * @param font Font to use for a label.
 * @param label Label on the button.
 * @param position Position of the buttons upper-left corner (in buffer coords).
 * @param color Color to draw the button (text + box).
 * @param scale Scale of the button relative to font size.
 * @param selected Determines whether to draw a box around the label.
 * @return Width of the button.
 */
uint8_t draw_button(Color *LCD_buffer, font_descriptor_t *font, const char *label, XY position, Color color, float scale, bool selected) {
    uint8_t width = draw_string(LCD_buffer, font, label, (XY){position.x + BUTTON_PADDING_X, position.y + BUTTON_PADDING_Y}, color, scale);
    width += 2 * BUTTON_PADDING_X;
    if (selected) draw_box(LCD_buffer, position, color, (XY){width, 2 * BUTTON_PADDING_Y + font->height * scale}, BUTTON_BORDER_WIDTH);
    return width;
}

/**
 * @brief Initializes current screen to represent menu_screen.
 */
void init_main_menu() {
    current_screen.handle_input_fn = &handle_input_main_menu;
    current_screen.draw_screen_fn = &draw_main_menu;
}

/**
 * @brief Initializes current screen to represent tutorial_screen.
 */
void init_tutorial_screen() {
    current_screen.handle_input_fn = &handle_input_tutorial_screen;
    current_screen.draw_screen_fn = &draw_tutorial_screen;
}

/**
 * @brief Initializes current screen to represent game_screen.
 */
void init_game_screen() {
    current_screen.handle_input_fn = &handle_input_game_screen;
    current_screen.draw_screen_fn = &draw_game_screen;
}

/**
 * @brief Initializes current screen to represent exit_screen.
 */
void init_exit_screen() {
    current_screen.handle_input_fn = &handle_input_exit_screen;
    current_screen.draw_screen_fn = &draw_exit_screen;
}

/**
 * @brief Updates current scene via input read from knobs,
 *        renders new visual representation of state to a buffer.
 * @param knobs Knob struct to read input from.
 * @param LCD_buffer Buffer to render current scene to.
 */
void update_screen(Knobs *knobs, Color *LCD_buffer) {
    current_screen.draw_screen_fn(LCD_buffer);
    current_screen.handle_input_fn(knobs);
}
