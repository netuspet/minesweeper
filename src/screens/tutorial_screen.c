/*******************************************************************
  tutorial_screen.c      - contains user manual

  This screen contains a description of the user interface.
  Only user action it provides is transition to main menu.

  @author Viktorie Valdmanova
  @author Petr Netusil

  Licensed under MIT license.

 *******************************************************************/

#include "tutorial_screen.h"

/* Button definition */
static void back_callback() {
    init_main_menu();
}

static const MenuButton back_to_menu_button = {"Got it, back to menu", -1, &back_callback};

/* Extern functions */

/**
 * @brief Calls callback upon pressing a button.
 * @param knobs Knobs struct to read input from.
 */
void handle_input_tutorial_screen(Knobs *knobs) {
    if (knobs->G_click) {
        back_to_menu_button.callback();
    }
}

/**
 * @brief Renders the screen contents into a buffer.
 * @param LCD_buffer A pointer to the buffer to draw to.
 */
void draw_tutorial_screen(Color *LCD_buffer) {
    draw_mono(LCD_buffer, BACKGROUND_COLOR);

    XY position = {MENU_LEFT_MARGIN, 0};

    position.y += UNPROPORTIONAL_FONT.height * BIG_FONT_SCALE + TEXT_MARGIN_TOP * BIG_FONT_SCALE;
    draw_string(LCD_buffer, &UNPROPORTIONAL_FONT, "How to play?", position, TEXT_COLOR, BIG_FONT_SCALE);
    position.y += UNPROPORTIONAL_FONT.ascent * NORMAL_FONT_SCALE;

    // red button
    position.x = MENU_LEFT_MARGIN;
    position.y += UNPROPORTIONAL_FONT.height * NORMAL_FONT_SCALE + TEXT_MARGIN_TOP * NORMAL_FONT_SCALE;
    position.x += draw_string(LCD_buffer, &UNPROPORTIONAL_FONT, "Use the ", position, TEXT_COLOR, NORMAL_FONT_SCALE);
    position.x += draw_string(LCD_buffer, &UNPROPORTIONAL_FONT, "red", position, RED, NORMAL_FONT_SCALE);
    draw_string(LCD_buffer, &UNPROPORTIONAL_FONT, " knob to move horizontally.", position, TEXT_COLOR, NORMAL_FONT_SCALE);
    position.x = LCD_WIDTH - 2 * MENU_LEFT_MARGIN;
    draw_icon(LCD_buffer, position, get_icon(MINE), NORMAL_FONT_SCALE);
    position.x = MENU_LEFT_MARGIN;
    position.y += UNPROPORTIONAL_FONT.ascent * NORMAL_FONT_SCALE;
    draw_string(LCD_buffer, &UNPROPORTIONAL_FONT, "Click to reveal a field.", position, TEXT_COLOR, NORMAL_FONT_SCALE);
    // blue button
    position.x = MENU_LEFT_MARGIN;
    position.y += (UNPROPORTIONAL_FONT.height + TEXT_MARGIN_TOP) * NORMAL_FONT_SCALE;
    position.x += draw_string(LCD_buffer, &UNPROPORTIONAL_FONT, "Use the ", position, TEXT_COLOR, NORMAL_FONT_SCALE);
    position.x += draw_string(LCD_buffer, &UNPROPORTIONAL_FONT, "blue", position, BLUE, NORMAL_FONT_SCALE);
    draw_string(LCD_buffer, &UNPROPORTIONAL_FONT, " knob to move vertically", position, TEXT_COLOR, NORMAL_FONT_SCALE);
    position.x = LCD_WIDTH - 2 * MENU_LEFT_MARGIN;
    draw_icon(LCD_buffer, position, get_icon(FLAG), NORMAL_FONT_SCALE);
    position.x = MENU_LEFT_MARGIN;
    position.y += UNPROPORTIONAL_FONT.ascent * NORMAL_FONT_SCALE;
    draw_string(LCD_buffer, &UNPROPORTIONAL_FONT, "Click to mark field with a flag.", position, TEXT_COLOR, NORMAL_FONT_SCALE);
    // green button
    position.x = MENU_LEFT_MARGIN;
    position.y += (UNPROPORTIONAL_FONT.height + TEXT_MARGIN_TOP) * NORMAL_FONT_SCALE;
    position.x += draw_string(LCD_buffer, &UNPROPORTIONAL_FONT, "Use the ", position, TEXT_COLOR, NORMAL_FONT_SCALE);
    position.x += draw_string(LCD_buffer, &UNPROPORTIONAL_FONT, "green", position, GREEN, NORMAL_FONT_SCALE);
    draw_string(LCD_buffer, &UNPROPORTIONAL_FONT, " knob to control the menu.", position, TEXT_COLOR, NORMAL_FONT_SCALE);

    // button
    position.x = MENU_LEFT_MARGIN;
    position.y += (UNPROPORTIONAL_FONT.height + TEXT_MARGIN_TOP) * NORMAL_FONT_SCALE;
    draw_button(LCD_buffer, &UNPROPORTIONAL_FONT, back_to_menu_button.label, position, TEXT_COLOR, NORMAL_FONT_SCALE, true);
}

