#ifndef MINESWEEPER_SCREEN_UTILS_H
#define MINESWEEPER_SCREEN_UTILS_H

#include "../peripherals/knobs/knob_utils.h"
#include "../peripherals/lcd/lcd_utils.h"
#include "../fonts/font_types.h"

typedef struct Screen {
    void (*handle_input_fn)(Knobs *);
    void (*draw_screen_fn)(Color *);
} Screen;

typedef struct MenuButton {
    const char *label;
    const int8_t value;
    void (*callback)();
} MenuButton;

typedef struct ButtonArea {
    const uint8_t row_amount;
    const uint8_t *columns_in_row;
    const MenuButton ***buttons;
} ButtonArea;

typedef struct CursorPosition {
    uint8_t x;
    uint8_t y;
} CursorPosition;

void move_cursor_x(CursorPosition *cursor, int8_t change, uint8_t max);

void move_cursor_y(CursorPosition *cursor, int8_t change, uint8_t max);

uint8_t draw_button(Color *LCD_buffer, font_descriptor_t *font, const char *label, XY position, Color color, float font_scale, bool selected);

void init_main_menu();

void init_tutorial_screen();

void init_game_screen();

void init_exit_screen();

void update_screen(Knobs *knobs, Color *LCD_buffer);

#endif//MINESWEEPER_SCREEN_UTILS_H
