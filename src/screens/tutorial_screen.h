#ifndef MINESWEEPER_TUTORIAL_SCREEN_H
#define MINESWEEPER_TUTORIAL_SCREEN_H

#include "../peripherals/knobs/knob_utils.h"
#include "../peripherals/lcd/icon.h"
#include "../peripherals/lcd/lcd_utils.h"
#include "../peripherals/lcd/text.h"
#include "../peripherals/led/led_utils.h"
#include "../visual_config.h"
#include "screen_utils.h"

void handle_input_tutorial_screen(Knobs *knobs);

void draw_tutorial_screen(Color *LCD_buffer);


#endif//MINESWEEPER_TUTORIAL_SCREEN_H
