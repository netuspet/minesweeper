#ifndef MINESWEEPER_KNOB_UTILS_H
#define MINESWEEPER_KNOB_UTILS_H

#include <stdbool.h>
#include <stdint.h>

#include "../mzapo_regs.h"

/* Masks and shifts for the periphery */
#define KNOB_VALUE_MASK 0xFF
#define R_KNOB_VALUE_SHIFT 16
#define G_KNOB_VALUE_SHIFT 8
#define B_KNOB_VALUE_SHIFT 0
#define KNOB_CLICK_MASK 0x1
#define R_KNOB_CLICK_SHIFT 26
#define G_KNOB_CLICK_SHIFT 25
#define B_KNOB_CLICK_SHIFT 24
#define KNOB_NOISE 3
#define KNOB_STEP 4     // NOISE + 1

typedef struct Knobs {
    volatile uint32_t *ptr;
    /* Previous state to detect update */
    uint8_t R_value;
    uint8_t G_value;
    uint8_t B_value;
    int8_t R_diff;      /* Accumulated difference in values */
    int8_t G_diff;
    int8_t B_diff;
    /* Computed update values */
    int8_t R_change;    /* Overflow of diff over KNOB_NOISE */
    int8_t G_change;
    int8_t B_change;
    bool R_click;
    bool G_click;
    bool B_click;
} Knobs;

void init_knobs(Knobs *knobs, uint8_t *mem_base);

void update_knobs(Knobs *knobs);

#endif//MINESWEEPER_KNOB_UTILS_H
