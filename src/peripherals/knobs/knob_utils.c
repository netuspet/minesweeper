/*******************************************************************
  knob_utils.c      - provides abstraction for working with knobs
                      peripheral

  @author Viktorie Valdmanova
  @author Petr Netusil

  Licensed under MIT license.

 *******************************************************************/

#include "knob_utils.h"


/**
 * @brief Initializes given Knobs struct.
 * @param knobs Knob struct to be initialized.
 * @param mem_base Pointer to memory mapping of the periphery.
 */
void init_knobs(Knobs *knobs, uint8_t *mem_base) {
    knobs->ptr = (volatile uint32_t *) (mem_base + SPILED_REG_KNOBS_8BIT_o);
    knobs->R_change = 0;
    knobs->G_change = 0;
    knobs->B_change = 0;
    knobs->R_click = 0;
    knobs->G_click = 0;
    knobs->B_click = 0;
}

/**
 * @brief Computes difference between old_value and new_value.
 *        Takes into account under/overflowing of the knob value.
 * @param old_value Old value of the knob.
 * @param new_value New value of the knob.
 * @return Computed knob change.
 */
static int8_t get_diff(uint8_t old_value, uint8_t new_value) {
    int8_t change = old_value - new_value;
    return change;
}

/**
 * @brief Handles overflowing of diff (accumulates change) into
 *        change.
 * @param change Pointer to knob diff.
 * @param diff Pointer to knob change.
 */
static void handle_change(int8_t *change, int8_t *diff) {
    if (*diff > KNOB_NOISE || *diff < -KNOB_NOISE) {
        *change = *diff / KNOB_STEP;
        *diff -= *change * KNOB_STEP;
    }
    else *change = 0;
}

/**
 * @brief Updates given Knobs struct,
 *        detects changes and clicks.
 * @param knobs
 */
void update_knobs(Knobs *knobs) {
    uint8_t R_value = knobs->R_value;
    uint8_t G_value = knobs->G_value;
    uint8_t B_value = knobs->B_value;

    knobs->R_value = (*knobs->ptr >> R_KNOB_VALUE_SHIFT) & KNOB_VALUE_MASK;
    knobs->G_value = (*knobs->ptr >> G_KNOB_VALUE_SHIFT) & KNOB_VALUE_MASK;
    knobs->B_value = (*knobs->ptr >> B_KNOB_VALUE_SHIFT) & KNOB_VALUE_MASK;

    knobs->R_diff += get_diff(R_value, knobs->R_value);
    knobs->G_diff += get_diff(G_value, knobs->G_value);
    knobs->B_diff += get_diff(B_value, knobs->B_value);

    handle_change(&knobs->R_change, &knobs->R_diff);
    handle_change(&knobs->G_change, &knobs->G_diff);
    handle_change(&knobs->B_change, &knobs->B_diff);

    knobs->R_click = (*knobs->ptr >> R_KNOB_CLICK_SHIFT & KNOB_CLICK_MASK) == 1;
    knobs->G_click = (*knobs->ptr >> G_KNOB_CLICK_SHIFT & KNOB_CLICK_MASK) == 1;
    knobs->B_click = (*knobs->ptr >> B_KNOB_CLICK_SHIFT & KNOB_CLICK_MASK) == 1;
}
