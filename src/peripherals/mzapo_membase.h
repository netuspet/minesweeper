#ifndef MINESWEEPER_MZAPO_MEMBASE_H
#define MINESWEEPER_MZAPO_MEMBASE_H

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stddef.h>

#include "mzapo_phys.h"
#include "mzapo_regs.h"

typedef struct MemBase {
    uint8_t *lcd_mem_base;
    uint8_t *phys_mem_base;
    uint8_t *pwm_mem_base;
} MemBase;

bool init_mem_base(MemBase *mem_base);

#endif
