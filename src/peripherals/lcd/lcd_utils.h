#ifndef MINESWEEPER_LCD_UTILS_H
#define MINESWEEPER_LCD_UTILS_H

#include "../lcd/mzapo_parlcd.h"
#include "../mzapo_membase.h"

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#define LCD_WIDTH 480
#define LCD_HEIGHT 320
#define LCD_BUFFER_INDEX(buff, x, y) (buff[y * LCD_WIDTH + x])

typedef uint16_t Color;

typedef struct XY {
    uint16_t x;
    uint16_t y;
} XY;

enum COLORS { RED = 0xF800,
              GREEN = 0x07E0,
              BLUE = 0x001F,
              YELLOW = 0xFFE0,
              ORANGE = 0xFC00,
              VIOLET = 0x8010,
              GRAY = 0x8410,
              LIGHTGRAY = 0xC618,
              LIGHTBLUE = 0x061F,
              BLACK = 0x0000,
              WHITE = 0xFFFF };


void render_buffer(Color *LCD_buffer, uint8_t *mem_base);

void draw_point(Color *LCD_buffer, XY position, Color color, uint16_t size);

void draw_point_opaque(Color *LCD_buffer, XY position, Color color, float opacity, uint16_t size);

void draw_mono(Color *LCD_buffer, Color color);

void draw_box(Color *LCD_buffer, XY position, Color color, XY size, uint16_t width);

#endif// MINESWEEPER_LCD_UTILS_H
