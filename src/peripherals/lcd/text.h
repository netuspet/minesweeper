//
// Created by viki on 9.5.22.
//

#ifndef MINESWEEPER_TEXT_H
#define MINESWEEPER_TEXT_H

#include <string.h>
#include <stdio.h>

#include "lcd_utils.h"
#include "../../fonts/font_types.h"

uint8_t get_char_width(font_descriptor_t *font, char c);

uint8_t draw_char(Color *LCD_buffer, font_descriptor_t *font, char c, XY position, Color color, float scale);

uint16_t draw_string(Color *LCD_buffer, font_descriptor_t *font, const char *str, XY position, Color color, float scale);

//uint16_t draw_string_frame(Color *LCD_buffer, font_descriptor_t *font, char *str, Color color, float scale, uint8_t frame_width, uint8_t padding) {
//
//}

#endif// MINESWEEPER_TEXT_H
