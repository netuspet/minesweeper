/*******************************************************************
  text.c      - provides basic function for working with fonts

  @author Viktorie Valdmanova
  @author Petr Netusil

  Licensed under MIT license.

 *******************************************************************/

#include "text.h"

/**
 * @brief Returns a width of a given character in a given font.
 * @param font Pointer to a font descriptor.
 * @param ch Char to get the width for.
 * @return Width of a character in pixels.
 */
uint8_t get_char_width(font_descriptor_t *font, char ch) {
    if (!font->width) return font->maxwidth;
    else
        return font->width[(int) ch];
}

/**
 * @brief Draws character into a buffer.
 * @param LCD_buffer Buffer to write to.
 * @param font Font (descriptor) to use.
 * @param ch Character to draw.
 * @param position Position of the upper left corner (in the buffer coords).
 * @param color Color to paint the character.
 * @param scale How much to scale the font, relative to its' size.
 * @return Width of the character in pixels.
 */
uint8_t draw_char(Color *LCD_buffer, font_descriptor_t *font, char ch, XY position, Color color, float scale) {
    // check if the given char is in the font
    if (ch < font->firstchar || ch >= font->size - font->firstchar) {
        fprintf(stderr, "Char '%c' is not supported by font %s\n", ch, font->name);
        return -1;
    }
    // get char's index in the font
    ch -= font->firstchar;
    uint8_t ch_width = get_char_width(font, ch);
    const font_bits_t *bits_ptr;
    if (font->offset) bits_ptr = &font->bits[font->offset[(int) ch]];
    else
        bits_ptr = &font->bits[ch * font->height];
    for (uint8_t y = 0; y < font->height; ++y) {
        font_bits_t bits_mask = *bits_ptr;
        for (uint8_t x = 0; x < ch_width; ++x) {
            if (bits_mask & 0x8000) draw_point(LCD_buffer, (XY){position.x + scale * x, position.y + scale * y}, color, scale);
            bits_mask <<= 1;
        }
        bits_ptr++;
    }
    return ch_width;
}

/**
 * @brief Draws a string into a buffer.
 * @param LCD_buffer Buffer to write to.
 * @param font Font (descriptor) to use.
 * @param str String to draw.
 * @param position Position of the upper left corner (in the buffer coords).
 * @param color Color to draw the string.
 * @param scale How much to scale the font, relative to its' size.
 * @return Width of the string (used area) in pixels.
 */
uint16_t draw_string(Color *LCD_buffer, font_descriptor_t *font, const char *str, XY position, Color color, float scale) {
    uint16_t x_start = position.x;
    for (int i = 0; i < strlen(str); ++i) {
        position.x += draw_char(LCD_buffer, font, str[i], position, color, scale) * scale;
    }
    return position.x - x_start;
}
