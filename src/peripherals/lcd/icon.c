/*******************************************************************
  icon.c      - provides icon data and rendering function

  Icons are defined as int8_t array with each value representing
  index of the color in a color map. -1 is reserved for TRANSPARENT.

  @author Viktorie Valdmanova
  @author Petr Netusil

  Licensed under MIT license.

 *******************************************************************/

#include "icon.h"

/* Mine icon */
static const int8_t mine_data[] = {-1, -1, -1, -1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 1, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, 0, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1};
static const Color mine_colors[] = {BLACK, ORANGE};
static const Icon mine = {
        .width = 13,
        .height = 20,
        .data = mine_data,
        .color_map = mine_colors};

/* Flag icon */
static const int8_t flag_data[] = {-1, -1, -1, -1, -1, -1, -1, -1, -1, 1, 1, 0, 0, -1, -1, -1, -1, -1, -1, -1, 1, 1, 1, 1, 0, 0, -1, -1, -1, -1, -1, 1, 1, 1, 1, 1, 1, 0, 0, -1, -1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, -1, -1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, -1, -1, -1, -1, -1, 1, 1, 1, 1, 1, 1, 0, 0, -1, -1, -1, -1, -1, -1, -1, 1, 1, 1, 1, 0, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, 1, 1, 0, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0};
static const Color flag_colors[] = {BLACK, RED};
static const Icon flag = {
        .width = 13,
        .height = 20,
        .data = flag_data,
        .color_map = flag_colors};

/**
 * Returns a reference to icon from its' name.
 * @param name Name of the requested icon.
 * @return Pointer to the static Icon.
 */
const Icon *get_icon(IconName name) {
    switch (name) {
        case MINE:
            return &mine;
        case FLAG:
            return &flag;
        default:
            return NULL;
    }
}

/**
 * Renders given icon into a given buffer.
 * @param LCD_buffer Buffer to render to.
 * @param position Position of the upper left corner (in buffer coords).
 * @param icon Pointer to the icon to be rendered.
 * @param scale Scaling factor.
 */
void draw_icon(Color *LCD_buffer, XY position, const Icon *icon, uint32_t scale) {
    for (uint8_t y = 0; y < icon->height; ++y) {
        for (uint8_t x = 0; x < icon->width; ++x) {
            int8_t value = icon->data[y * icon->width + x];
            if (value != TRANSPARENT_COLOR_CODE) {
                draw_point(LCD_buffer, (XY){position.x + scale * x, position.y + scale * y}, icon->color_map[value], scale);
            }
        }
    }
}