#ifndef MINESWEEPER_ICON_H
#define MINESWEEPER_ICON_H

#include <stdint.h>
#include "lcd_utils.h"

#define TRANSPARENT_COLOR_CODE -1

typedef enum IconName { MINE, FLAG } IconName;

typedef struct Icon {
    const int8_t *data;
    const uint8_t width;
    const uint8_t height;
    const Color *color_map;
} Icon;

const Icon *get_icon(IconName name);

void draw_icon(Color *LCD_buffer, XY position, const Icon *icon, uint32_t scale);

#endif//MINESWEEPER_ICON_H
