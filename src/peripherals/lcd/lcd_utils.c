/*******************************************************************
  lcd_utils.c      - provides basic functions for working with LCD

  @author Viktorie Valdmanova
  @author Petr Netusil

  Licensed under MIT license.

 *******************************************************************/

#include "lcd_utils.h"

/**
 * @brief Writes data from a buffer to the periphery.
 * @param LCD_buffer Buffer to write data from.
 * @param mem_base MEmory base of the periphery.
 */
void render_buffer(Color *LCD_buffer, uint8_t *mem_base) {
    parlcd_write_cmd(mem_base, 0x2c);
    for (int32_t i = 0; i < LCD_WIDTH * LCD_HEIGHT; i += 2) {
        parlcd_write_data2x(mem_base, *(uint32_t *)(LCD_buffer+ i));
    }
}

/**
 * @brief Draws point (or a square) into a buffer.
 * @param LCD_buffer Buffer to write to.
 * @param position Position of the upper left corner in display coordinates.
 * @param color Color of the point.
 * @param size Size of the point.
 */
void draw_point(Color *LCD_buffer, XY position, Color color, uint16_t size) {
    for (int32_t y = position.y; y < position.y + size; ++y) {
        for (int32_t x = position.x; x < position.x + size; ++x) {
           LCD_buffer[y * LCD_WIDTH + x] = color;
        }
    }
}

static Color add_colors(Color base, Color addition, float opacity) {
    Color R_base = RED & base;
    Color G_base = GREEN & base;
    Color R_addition = RED & addition;
    Color G_addition = GREEN & addition;
    Color B_addition = BLUE & addition;
    Color B_base = BLUE & base;
    R_base *= opacity;
    G_base *= opacity;
    B_base *= opacity;
    R_addition *= (1.0f - opacity);
    G_addition *= (1.0f - opacity);
    B_addition *= (1.0f - opacity);
    uint32_t R_out = R_base + R_addition;
    uint32_t G_out = G_base + G_addition;
    uint32_t B_out = B_base + B_addition;
    R_out = (R_out > RED) ? RED : R_out;
    G_out = (G_out > GREEN) ? GREEN : G_out;
    B_out = (B_out > BLUE) ? BLUE : B_out;
    Color out = R_out | G_out | B_out;
    return out;
}

/**
 * @brief Draws an opaque point (or a square) into a buffer.
 * @param LCD_buffer Buffer to write to.
 * @param position Position of the upper left corner in display coordinates.
 * @param color Color of the point.
 * @param opacity
 * @param size Size of the point.
 */
void draw_point_opaque(Color *LCD_buffer, XY position, Color color, float opacity, uint16_t size) {
    for (int32_t y = position.y; y < position.y + size; ++y) {
        for (int32_t x = position.x; x < position.x + size; ++x) {
            LCD_buffer[y * LCD_WIDTH + x] = add_colors(LCD_buffer[y * LCD_WIDTH + x], color, opacity);
        }
    }
}

/**
 * @brief Sets all the pixels in a buffer to given color.
 * @param LCD_buffer Buffer to write to.
 * @param color Color to set the buffer.
 */
void draw_mono(Color *LCD_buffer, Color color) {
    for (int i = 0; i < LCD_WIDTH * LCD_HEIGHT; ++i) LCD_buffer[i] = color;
}

/**
 * @brief Draws a box (only border) into a buffer.
 * @param LCD_buffer Buffer to write to.
 * @param position Position of the upper left corner in display coordinates.
 * @param color Color of the box.
 * @param size Size of the box.
 * @param line_width Width of the line.
 */
void draw_box(Color *LCD_buffer, XY position, Color color, XY size, uint16_t line_width) {
    // top side
    for (uint16_t y = position.y; y < position.y + line_width; ++y) {
        for (uint16_t x = position.x; x < position.x + size.x; ++x) {
            LCD_BUFFER_INDEX(LCD_buffer, x, y) = color;
        }
    }
    // left and right side
    for (uint16_t y = position.y + line_width; y < position.y + size.y - line_width; ++y) {
        for (uint16_t x = position.x; x < position.x + line_width; ++x) {
            LCD_BUFFER_INDEX(LCD_buffer, x, y) = color;
        }
        for (uint16_t x = position.x + size.x - line_width; x < position.x + size.x; ++x) {
            LCD_BUFFER_INDEX(LCD_buffer, x, y) = color;
        }
    }
    // bottom side
    for (uint16_t y = position.y + size.y - line_width; y < position.y + size.y; ++y) {
        for (uint16_t x = position.x; x < position.x + size.x; ++x) {
            LCD_BUFFER_INDEX(LCD_buffer, x, y) = color;
        }
    }
}
