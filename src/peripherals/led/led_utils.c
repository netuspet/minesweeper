/*******************************************************************
  led_utils.c      - provides basic functions and abstraction for
                     working with RGB diodes and led line

  @author Viktorie Valdmanova
  @author Petr Netusil

  Licensed under MIT license.

 *******************************************************************/

#include "led_utils.h"

typedef struct LED {
    volatile uint32_t *line;
    volatile uint32_t *rgb[LED_RGB_AMOUNT];
} LED;

static LED leds;

/**
 * @brief Initializes the periphery.
 * @param mem_base Pointer to the memory mapping of the base register.
 */
void init_leds(uint8_t *mem_base) {
    leds.line = (volatile uint32_t *) (mem_base + SPILED_REG_LED_LINE_o);
    leds.rgb[0] = (volatile uint32_t *) (mem_base + SPILED_REG_LED_RGB1_o);
    leds.rgb[1] = (volatile uint32_t *) (mem_base + SPILED_REG_LED_RGB2_o);
}

/**
 * @brief Sets LED line to a given value.
 *        32 diodes are set on/off based on 32bit value.
 * @param amount Value to display by the line.
 */
void set_led_line(uint32_t amount) {
    *leds.line = amount;
}

/**
 * @brief Sets one of the RGB diodes to a given color.
 * @param idx Index of the diode to set.
 * @param value Value (24bit coded color) to set the diode to.
 */
void set_rgb(uint8_t idx, uint32_t value) {
    *leds.rgb[idx] = value;
}

/**
 * @brief Set a diode to value for a given duration (blocking),
 *        then turn it off.
 * @param value 24bit coded color to blink.
 * @param duration_ms Duration of the blink.
 */
void blink_rgbs(uint32_t value, uint16_t duration_ms) {
    set_rgb(0, value);
    set_rgb(1, value);
    struct timespec delay = {duration_ms / 1000, (duration_ms % 1000) * 1000 * 1000};
    clock_nanosleep(CLOCK_MONOTONIC, 0, &delay, NULL);
    set_rgb(0, 0);
    set_rgb(1, 0);
}

/**
 * @brief Hardcoded effect to start with the line at 0xFFFFFFFF and
 *        symmetrically turn of diodes one by one from center.
 * @param iterations How many diodes to turn off + 1 for initial frame.
 *                   The effect is completed after 17 iterations.
 * @param duration_ms Duration of a single frame in the animation.
 */
void line_from_in_out(uint8_t iterations, uint16_t duration_ms) {
    struct timespec delay = {duration_ms / 1000, (duration_ms % 1000) * 1000 * 1000};
    uint16_t left = 0xFFFF;
    uint16_t right = 0xFFFF;
    for (int i = 0; i < iterations; ++i) {
        set_led_line((uint32_t) left << 16 | right);
        clock_nanosleep(CLOCK_MONOTONIC, 0, &delay, NULL);
        left <<= 1;
        right >>= 1;
    }
}

/**
 * @brief Hardcoded effect to start with the line at 0x0, followed by
 *        symmetrically turning on diodes one by one from edges.
 * @param iterations How many diodes to turn on + 1 for initial frame.
 *                   The effect is completed after 17 iterations.
 * @param duration_ms Duration of a single frame in the animation.
 */
void line_from_out_in(uint8_t iterations, uint16_t duration_ms) {
    struct timespec delay = {duration_ms / 1000, (duration_ms % 1000) * 1000 * 1000};
    uint32_t left = 0xFFFF8000;
    uint32_t right = 0x0001FFFF;
    for (int i = 0; i < iterations; ++i) {
        set_led_line(left << 16 | right >> 16);
        clock_nanosleep(CLOCK_MONOTONIC, 0, &delay, NULL);
        left >>= 1;
        right <<= 1;
    }
}

