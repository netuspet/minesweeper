#ifndef MINESWEEPER_LED_UTILS_H
#define MINESWEEPER_LED_UTILS_H

#include <stdint.h>
#include <time.h>

#define LED_RGB_AMOUNT 2

#include "../mzapo_regs.h"


void init_leds(uint8_t *mem_base);

void set_led_line(uint32_t amount);

void set_rgb(uint8_t idx, uint32_t value);

void line_from_in_out(uint8_t iterations, uint16_t duration);

void line_from_out_in(uint8_t iterations, uint16_t duration);

void blink_rgbs(uint32_t value, uint16_t duration_ms);

#endif//MINESWEEPER_LED_UTILS_H
