/*******************************************************************
  mzapo_membase.c     - file to group memory mappings of peripherals

  @author Viktorie Valdmanova
  @author Petr Netusil

  Licensed under MIT license.

 *******************************************************************/


#include "mzapo_membase.h"

/**
 * Map individual peripheries into memory and store pointers into a MemBase struct.
 * @param mem_base MemBase struct to save pointers to.
 * @return true if mapping succeeded, else false.
 */
bool init_mem_base(MemBase *mem_base) {
    mem_base->lcd_mem_base = (uint8_t *) map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
    mem_base->phys_mem_base = (uint8_t *) map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    mem_base->pwm_mem_base = (uint8_t *) map_phys_address(AUDIOPWM_REG_BASE_PHYS, AUDIOPWM_REG_SIZE, 0);
    if (mem_base->lcd_mem_base == NULL || mem_base->phys_mem_base == NULL || mem_base->pwm_mem_base == NULL) {
        return false;
    }
    return true;
}